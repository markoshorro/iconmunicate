package se.chalmers.edu.iconmunicate.util;

import android.content.res.XmlResourceParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import se.chalmers.edu.iconmunicate.model.Pictogram;

/**
 * Created by Inès on 26-Dec-15.
 */
public class XMLParser {

    private static final String ns = null;
    private static final String resources_tag = "resources";
    private static final String pictogram_tag = "pictogram";
    private static final String category_tag = "category";
    private static final String url_tag = "url";
    private static final String description_tag = "description";
    private static final String tag_tag = "tag";
    private static final String language_tag = "language";
    private static final String text_tag = "text";

    public ArrayList<Pictogram> parse(XmlResourceParser parser) throws XmlPullParserException, IOException {
        parser.next();
        parser.next();
        return readFile(parser);
    }

    private ArrayList<Pictogram> readFile(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Pictogram> pictograms = new ArrayList<Pictogram>();

        parser.require(XmlPullParser.START_TAG, ns, resources_tag);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals(pictogram_tag)) {
                pictograms.add(readPictogram(parser));
            } else {
                skip(parser);
            }
        }
        return pictograms;
    }

    /*Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them
    * off to their respective "read" methods for processing. Otherwise, skips the tag.
    */
    private Pictogram readPictogram(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, pictogram_tag);
        Pictogram.Category category = null;
        String url = null;
        ArrayList<String> tags = new ArrayList<String>();
        HashMap<Constants.Language, String> descriptions = new HashMap<Constants.Language, String>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(category_tag)) {
                category = readCategory(parser);
            } else if (name.equals(url_tag)) {
                url = readUrl(parser);
            } else if (name.equals(tag_tag)) {
                tags.add(readTag(parser));
            } else if (name.equals(description_tag)) {
                descriptions = readDescription(parser, descriptions);
            } else {
                skip(parser);
            }
        }
        return new Pictogram(category, url, tags, descriptions);
    }

    private Pictogram.Category readCategory(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, category_tag);
        String category = readTextValue(parser);
        parser.require(XmlPullParser.END_TAG, ns, category_tag);
        return Pictogram.Category.valueOf(category);
    }

    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, url_tag);
        String url = readTextValue(parser);
        parser.require(XmlPullParser.END_TAG, ns, url_tag);
        return url;
    }

    private String readTag(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag_tag);
        String language = null;
        String text = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(language_tag)) {
                language = readLanguage(parser);
            } else if (name.equals(text_tag)) {
                text = readText(parser).toLowerCase();
            } else {
                skip(parser);
            }
        }
        return language + Pictogram.LANGUAGE_SEPARATOR + text;
    }

    private String readLanguage(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, language_tag);
        String language = readTextValue(parser);
        parser.require(XmlPullParser.END_TAG, ns, language_tag);
        return language;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, text_tag);
        String text = readTextValue(parser);
        parser.require(XmlPullParser.END_TAG, ns, text_tag);
        return text;
    }

    private HashMap<Constants.Language, String> readDescription(XmlPullParser parser, HashMap<Constants.Language, String> desc) throws IOException, XmlPullParserException {
        HashMap<Constants.Language, String> descriptions = new HashMap<Constants.Language, String>(desc);
        parser.require(XmlPullParser.START_TAG, ns, description_tag);
        Constants.Language language = null;
        String text = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(language_tag)) {
                language = Constants.Language.valueOf(readLanguage(parser));
            } else if (name.equals(text_tag)) {
                text = readText(parser);
            } else {
                skip(parser);
            }
        }
        descriptions.put(language, text);
        return descriptions;
    }

    private String readTextValue(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
