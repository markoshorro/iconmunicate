package se.chalmers.edu.iconmunicate;

/**
 * Some code has been taken from:
 * https://github.com/googlesamples/android-nearby/tree/master/messages/NearbyDevices
 *
 * However, we do not follow the same scheme with fragments
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.controller.ConnectionManager;
import se.chalmers.edu.iconmunicate.controller.DatabaseManager;
import se.chalmers.edu.iconmunicate.controller.NotificationsManager;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.settings.SettingsActivity;
import se.chalmers.edu.iconmunicate.util.AboutDialog;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;
import se.chalmers.edu.iconmunicate.view.ConversationListAdapter;
import se.chalmers.edu.iconmunicate.view.DevicesListDialog;

/**
 * This main class handles the list of conversations, discovers devices and shares information
 * It is not a God class, but it is on charge of the connection, so it is critical
 */
public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    /**
     * For debugging purposes
     */
    private static final String TAG = "MainActivity";

    /**
     * Buttons: for discovering (subscribe) and sharing (publish)
     */
    private MenuItem mNearbyImageButton;

    /**
     * Tracks if we are currently resolving an error related to Nearby permissions. Used to avoid
     * duplicate Nearby permission dialogs if the user initiates both subscription and publication
     * actions without having opted into Nearby.
     */
    private boolean mResolvingNearbyPermissionError = false;

    /**
     * Fragment manager
     */
    private FragmentManager fragManager;

    /**
     * Device list dialog. It is handled with fragManager
     */
    private DevicesListDialog devicesDialog;

    /**
     * About dialog. It is handled with fragManager
     */
    private AboutDialog aboutDialog;

    /**
     * Conversation list
     */
    private ListView mConversationListView;
    private ArrayList<Conversation> mConversationList = new ArrayList<>();
    private ConversationListAdapter mConversationAdapter;

    /**
     * Database helper
     */
    private DatabaseManager mDatabaseManager;

    /**
     * When getActivity is no available
     */
    private Activity mActivity;
    private Gson gson = new Gson();
    private static String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // Initializing
        mActivity = this;
        Common.getInstance().setContext(getApplicationContext());
        setContentView(R.layout.activity_main);
        android_id = Settings.Secure.getString(this.getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        // Best strategy ever to manage the fragments. Thanks stackoverflow: you're the real MVP
        fragManager = getSupportFragmentManager();
        aboutDialog = new AboutDialog();

        // Initializing databasemanager
        mDatabaseManager = DatabaseManager.getInstance();
        mDatabaseManager.init(this);

        // Getting devices dialog
        devicesDialog = DevicesListDialog.getInstance();

        // Initializing ConnectionManager
        ConnectionManager.getInstance().setId(android_id);
        ConnectionManager.getInstance().setActivity(this);
        ConnectionManager.getInstance().init(getApplicationContext());

        // Setting Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Notification manager
        NotificationsManager.getInstance().setContext(getApplicationContext());

        // Release version adapters
        mConversationList = mDatabaseManager.getAllConversations();
        mConversationListView = (ListView) findViewById(R.id.conversation_list);
        mConversationAdapter = new ConversationListAdapter(this,
                mConversationList, getLayoutInflater());

        mConversationListView.setAdapter(mConversationAdapter);
        mConversationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Conversation device = (Conversation) mConversationAdapter.getItem(position);
                        runOnUiThread(_openConversation(device));
                    }
                }).start();

            }
        });

        mConversationListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           final int position, long id) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            Conversation conversation = mConversationList.get(position);
                            mDatabaseManager.deleteConversation(conversation.getId());
                            mConversationAdapter.updateList(mDatabaseManager.getAllConversations());
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setMessage(R.string.detele_conversation)
                        .setPositiveButton(R.string.dialog_confirm, dialogClickListener)
                        .setNegativeButton(R.string.dialog_cancel, dialogClickListener).show();
                return true;
            }
        });

        mConversationListView.setItemsCanFocus(false);
    }

    /**
     * Private method to open the Conversation Activity
     */
    private Runnable _openConversation(final Conversation device) {
        try {
            final Intent intent = new Intent(this, ConversationActivity.class);
            return new Runnable() {
                @Override
                public void run() {
                    Common.resetNewMessages(device.getId());
                    Common.updateSharedPreference(Constants.KEY_ACT_CONVERSATION, Constants.ACTIVITY_RUN);
                    intent.putExtra(Constants.EXTRA_DEV_INFO, gson.toJson(device));
                    startActivity(intent);
                }
            };
        } catch (final Exception e) {
            e.printStackTrace();
            return new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),
                            R.string.error_unexpected, Toast.LENGTH_LONG).show();
                }
            };
        }
    }

    /**
     * Private method to run settings
     */
    private Runnable _openSettings() {
        try {
            final Intent intent = new Intent(this, SettingsActivity.class);
            return new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                }
            };
        } catch (final Exception e) {
            e.printStackTrace();
            return new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),
                            R.string.error_unexpected, Toast.LENGTH_LONG).show();
                }
            };
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Not fancy way at all but...
        mNearbyImageButton = menu.getItem(0);
        updateUI();
        return true;
    }

    /**
     * Do discover and publish
     */
    public boolean doPublishDiscover() {
        String visibility = Common.getValue(Constants.KEY_VISIBLE);
        if (TextUtils.equals(visibility, Constants.STATE_VISIBLE)) {
            Common.updateSharedPreference(Constants.KEY_VISIBLE, Constants.STATE_NOT_VISIBLE);
        } else {
            Common.updateSharedPreference(Constants.KEY_VISIBLE, Constants.STATE_VISIBLE);
        }
        return true;
    }

    /**
     * Select action depending on the item
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_nearby:
                return doPublishDiscover();
            case R.id.action_list:
                showDevicesDialog();
                return true;
            case R.id.action_about:
                aboutDialog.show(fragManager, "aboutDialog");
                return true;
            case R.id.action_settings:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(_openSettings());
                    }
                }).start();
                return true;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Private method to show the dialog with the devices
     */
    private void showDevicesDialog() {
        String visibility = Common.getValue(Constants.KEY_VISIBLE);

        if (TextUtils.equals(visibility, Constants.STATE_VISIBLE)) {
            devicesDialog.show(fragManager, "devicesDialog");
        } else {
            Toast.makeText(this, R.string.error_devices_dialog, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * If something changes on the Shared Preferecens, this method is executed
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, final String key) {
        Log.d(TAG, "onSharedPreferenceChanged" + TAG + ": " + key);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.equals(key, Constants.KEY_VISIBLE)) {
                    String visibility = Common.getValue(key);
                    if (TextUtils.equals(visibility, Constants.STATE_VISIBLE)) {
                        ConnectionManager.getInstance().publishDiscover();
                        ConnectionManager.getInstance().subscribe();
                    } else {
                        ConnectionManager.getInstance().unpublishAll();
                        ConnectionManager.getInstance().unsubscribe();
                    }
                }
                updateUI();
            }
        });
    }

    /**
     * Updates the UI when the state of a subscription or publication action changes.
     */
    private void updateUI() {
        String updateConvList = Common.getValue(Constants.KEY_UPDATE_CONVERSATION_LIST);
        String visibility = Common.getValue(Constants.KEY_VISIBLE);
        Log.d(TAG, "updateUI. Instance: " + this + ". value: " + updateConvList);

        if (mNearbyImageButton != null) {
            mNearbyImageButton.setIcon(
                    TextUtils.equals(visibility, Constants.STATE_VISIBLE) ?
                            R.drawable.ic_cancel_white_24dp : R.drawable.ic_nearby_w);
        }

        if ((mConversationAdapter != null) &&
                (TextUtils.equals(updateConvList, Constants.TASK_UPDATE))) {
            Log.d(TAG, "updating conversation list");
            // update list
            updateConversationList();
            // Update shared preferences
            Common.updateSharedPreference(Constants.KEY_UPDATE_CONVERSATION_LIST,
                    Constants.TASK_NONE);
        }
    }

    /**
     * Helper method to update conversation list
     */
    private void updateConversationList() {
        mConversationAdapter.updateList(mDatabaseManager.getAllConversations());
        mConversationList = mDatabaseManager.getAllConversations();
        mConversationAdapter.notifyDataSetChanged();
    }

    /**
     * Managing errors
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }

    // Helper function
    public static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "connection to GoogleApiClient failed");
    }

    /**
     * Lifecycle
     */
    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean previouslyStarted = prefs.getBoolean(Constants.PREF_FIRST_LAUNCH, false);
        if(!previouslyStarted) {
            mDatabaseManager = DatabaseManager.getInstance();
            Intent intent = new Intent(this, FirstLaunchActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();

        updateUI();
        ConnectionManager.getInstance().setActivity(this);
        // Set up a listener whenever a key changes
        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
        //ConnectionManager.getInstance().connect();
        updateConversationList();
    }

    @Override
    //TODO: implement modes for battery, i.e, if we want to stop Nearby or not when hiding activity
    public void onStop() {
        Log.d(TAG, "onStop");
        String taskConversationActivity = Common.getValue(Constants.KEY_ACT_CONVERSATION);

        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .unregisterOnSharedPreferenceChangeListener(this);

        // We do not want to stop if conversation activity was launched or settings
        if (TextUtils.equals(taskConversationActivity, Constants.ACTIVITY_RUN)) {
            Log.d(TAG, "activity running...");
            super.onStop();
            return;
        }

        if (ConnectionManager.getInstance().isConnected() && !this.isChangingConfigurations()) {
            // Using Nearby is battery intensive. To preserve battery, stop subscribing or
            // publishing when the fragment is inactive.
            Log.d(TAG, "stopping everything");
            ConnectionManager.getInstance().unsubscribe();
            ConnectionManager.getInstance().unpublishAll();
            ConnectionManager.getInstance().disconnect();
        }
        super.onStop();
        return;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}