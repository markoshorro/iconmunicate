package se.chalmers.edu.iconmunicate.model;

import android.os.Build;

import com.google.android.gms.nearby.messages.Message;
import com.google.gson.Gson;

import java.nio.charset.Charset;

/**
 * Used to prepare the payload for a
 * {@link com.google.android.gms.nearby.messages.Message Nearby Message}. Adds a unique id (an
 * InstanceID) to the Message payload, which helps Nearby distinguish between multiple devices with
 * the same model name.
 */
public class DeviceMessage {
    private static final Gson gson = new Gson();

    private final String mReceiver;
    private final String mInstanceId;
    private final String mMessageBody;
    private final boolean discoverMsg; // true if discover message

    /**
     * Builds a new message. If discoverMsg true, then it is a message for discovering
     */
    public static Message newNearbyMessage(String instanceId, String mMessageBody,
                                           boolean discoverMsg) {
        DeviceMessage deviceMessage = new DeviceMessage(instanceId, mMessageBody, discoverMsg);
        return new Message(gson.toJson(deviceMessage).getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Regular message
     * @param instanceId sender
     * @param mReceiver receiver
     * @param mMessageBody body
     * @return
     */
    public static Message newNearbyMessage(String instanceId, String mMessageBody,
                                           String mReceiver) {
        DeviceMessage deviceMessage = new DeviceMessage(instanceId, mMessageBody,
                mReceiver, false);
        return new Message(gson.toJson(deviceMessage).getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Creates a {@code DeviceMessage} object from the string used to construct the payload to a
     * {@code Nearby} {@code Message}.
     */
    public static DeviceMessage fromNearbyMessage(Message message) {
        String nearbyMessageString = new String(message.getContent()).trim();
        return gson.fromJson(
                (new String(nearbyMessageString.getBytes(Charset.forName("UTF-8")))),
                DeviceMessage.class);
    }

    private DeviceMessage(String instanceId, boolean discoverMsg) {
        this.mInstanceId = instanceId;
        this.discoverMsg = discoverMsg;
        this.mMessageBody = Build.MODEL;
        this.mReceiver = null;
    }

    private DeviceMessage(String instanceId, String messageBody, boolean discoverMsg) {
        this.mInstanceId = instanceId;
        this.discoverMsg = discoverMsg;
        this.mMessageBody = messageBody;
        this.mReceiver = null;
    }

    private DeviceMessage(String mInstanceId, String mMessageBody, String mReceiver, boolean discoverMsg) {
        this.mInstanceId = mInstanceId;
        this.mReceiver = mReceiver;
        this.discoverMsg = discoverMsg;
        this.mMessageBody = mMessageBody;
    }

    public String getMessageBody() {
        return mMessageBody;
    }
    public String getmInstanceId() {
        return mInstanceId;
    }
    public boolean isDiscoverMsg() {
        return discoverMsg;
    }
    public String getmReceiver() {return mReceiver; }

}