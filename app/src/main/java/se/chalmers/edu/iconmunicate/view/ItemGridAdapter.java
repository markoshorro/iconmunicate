package se.chalmers.edu.iconmunicate.view;

/**
 * Created by Markos Horro on 29/12/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Based on Gabriele Mariotti (gabri.mariotti@gmail.com) implementation
 */
public class ItemGridAdapter extends RecyclerView.Adapter<ItemGridAdapter.SimpleViewHolder> {
    private static final int COUNT = Constants.N_ICS;

    /**
     * TAG to debug
     */
    private static final String TAG = "itemGridAdapter";

    private static Context mContext;
    private final List<Integer> mItems;
    private int mCurrentItemId = 0;

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public int iconId;

        public SimpleViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.item_grid);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText input = (EditText) ((Activity)mContext).findViewById(R.id.conversation_input_text);

                    ArrayList<String> picsArray =  new ArrayList<>(Arrays.asList(input.getText()
                            .toString().split(";")));

                    if (picsArray.size()>=Constants.MAX_IC_MSG) {
                        Toast.makeText(mContext, R.string.error_max_ic_msg, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int lineHeight = input.getLineHeight();
                    int drawableId = findIdByString(Constants.IC_SET + iconId);
                    Drawable drawable = ContextCompat.getDrawable(mContext, drawableId);
                    drawable.setBounds(0,0,lineHeight, lineHeight);
                    ImageSpan imageSpan = new ImageSpan(drawable);

                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    builder.append(input.getText());

                    // this is a string that will let you find a place, where the ImageSpan is.
                    String imgId = "[pic:" + iconId + "];";

                    // current selection is replaceв with imageId
                    builder.append(imgId);

                    builder.setSpan(imageSpan,
                            builder.length() - imgId.length(), builder.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    input.setText(builder);
                    // End of selection
                    input.setSelection(input.getText().length());
                    // To show snackbar to delete icons
                    Common.updateSharedPreference(Constants.KEY_INPUT_IC, Constants.TASK_UPDATE);
                }
            });
        }
    }

    public ItemGridAdapter(Context context) {
        mContext = context;
        mItems = new ArrayList<>(COUNT);
        for (int i = 0; i < COUNT; i++) {
            addItem(i);
        }
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_grid, parent, false);
        return new SimpleViewHolder(view);
    }

    public static int findIdByString(String string) {
        String packageName = mContext.getPackageName();
        int drawableId = mContext.getResources().getIdentifier(string,
                "drawable", packageName);
        return drawableId;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        //holder.title.setText(mItems.get(position).toString());
        int drawableId = findIdByString(Constants.IC_SET + (position + 1));
        Drawable drawable = ContextCompat.getDrawable(mContext, drawableId);
        drawable.setBounds(0,0,200, 200);
        SpannableStringBuilder builder = new SpannableStringBuilder();

        ImageSpan img = new ImageSpan(drawable, "myImage");
        builder.append(" ");
        builder.setSpan(img,
                0, builder.length(), 0);
        holder.title.setText(builder);
        holder.iconId = position + 1;
    }

    public void addItem(int position) {
        final int id = mCurrentItemId++;
        mItems.add(position, id);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
