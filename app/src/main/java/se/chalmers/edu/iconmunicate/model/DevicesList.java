package se.chalmers.edu.iconmunicate.model;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Markos Horro on 13/12/15.
 */
public class DevicesList {

    /**
     * Singleton pattern: this way different activies and/or fragments can use the same list
     */
    static private final DevicesList mInstrance = new DevicesList();
    static public DevicesList getDevicesList() { return mInstrance; }

    // Devices list
    private ArrayList<Conversation> devicesList;

    /**
     * Main constructor
     */
    public DevicesList() {
        devicesList = new ArrayList<>();
    }

    /**
     * Add device. If the device appears already on the list, then it is not added
     * @param device to add to the list
     */
    public void addDevice(Conversation device) {
        // We only add a device once
        for(int i = 0; i < devicesList.size(); i++) {
            if (TextUtils.equals(device.getId(),devicesList.get(i).getId())) {
                return;
            }
        }
        devicesList.add(device);
    }

    /**
     * Remove by user name
     */
    public void removeDevice(String deviceId) {
        for (int i = 0; i < devicesList.size(); i++) {
            if (TextUtils.equals(devicesList.get(i).getId(), deviceId)) {
                devicesList.remove(i);
                return;
            }
        }
    }

    public void replaceList(ArrayList<Conversation> list) {
        devicesList = list;
    }

    public ArrayList<Conversation> getAllDevices() {
        return devicesList;
    }

    public void clearList() {
        devicesList = new ArrayList<>();
    }

}
