package se.chalmers.edu.iconmunicate.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.model.Message;
import se.chalmers.edu.iconmunicate.model.Pictogram;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by Inès on 09-Jan-16.
 */

/**
 * Based on Gabriele Mariotti (gabri.mariotti@gmail.com) implementation
 */
public class ItemGridAdapterSearch extends RecyclerView.Adapter<ItemGridAdapterSearch.SimpleViewHolder> {

    /**
     * TAG to debug
     */
    private static final String TAG = "itemGridAdapterSearch";

    private static Context mContext;
    private static ArrayList<String> words;
    private static ArrayList<Pictogram> pictograms;
    private final List<Integer> mItems;
    private int mCurrentItemId = 0;

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public int current;

        public SimpleViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.item_grid);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText input = (EditText) ((Activity)mContext).findViewById(R.id.conversation_input_text);

                    ArrayList<String> picsArray =  new ArrayList<>(Arrays.asList(input.getText()
                            .toString().split(";")));

                    if (picsArray.size()>=Constants.MAX_IC_MSG) {
                        Toast.makeText(mContext, R.string.error_max_ic_msg, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int lineHeight = input.getLineHeight();
                    String imgId;
                    Drawable drawable;

                    if(current < words.size()){
                        String word = words.get(current);

                        Bitmap bitmap = textAsBitmap(word);
                        drawable = new BitmapDrawable(bitmap);

                        drawable.setBounds(0,0,lineHeight*(word.length()/2), lineHeight);

                        // this is a string that will let you find a place, where the ImageSpan is.
                        imgId = "[" + Message.TEXT_FORMAT + Message.FORMAT_SEPARATOR + word + "];";
                    } else {
                        Pictogram pictogram = pictograms.get(current - words.size());
                        int drawableId = findIdByString(pictogram.getUrl());
                        drawable = ContextCompat.getDrawable(mContext, drawableId);
                        drawable.setBounds(0, 0, lineHeight, lineHeight);

                        // this is a string that will let you find a place, where the ImageSpan is.
                        imgId = "[" + Message.PICTOGRAM_FORMAT + Message.FORMAT_SEPARATOR + pictogram.getId() + "];";
                    }

                    ImageSpan imageSpan = new ImageSpan(drawable);

                    SpannableStringBuilder builder = new SpannableStringBuilder();
                    builder.append(input.getText());

                    // current selection is replaced with imageId
                    builder.append(imgId);

                    builder.setSpan(imageSpan,
                            builder.length() - imgId.length(), builder.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    input.setText(builder);
                    // End of selection
                    input.setSelection(input.getText().length());
                    // To show snackbar to delete icons
                    Common.updateSharedPreference(Constants.KEY_INPUT_IC, Constants.TASK_UPDATE);
                }
            });
        }
    }

    public ItemGridAdapterSearch(Context context, ArrayList<Pictogram> pictograms, ArrayList<String> words) {
        mContext = context;
        this.pictograms = new ArrayList<>(pictograms);
        this.words = new ArrayList<>(words);
        int count = pictograms.size() + words.size();
        mItems = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            addItem(i);
        }
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_grid, parent, false);
        return new SimpleViewHolder(view);
    }

    public static int findIdByString(String string) {
        String packageName = mContext.getPackageName();
        int drawableId = mContext.getResources().getIdentifier(string,
                "drawable", packageName);
        return drawableId;
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {

        Drawable drawable;

        if(position < words.size()){
            String word = words.get(position);

            Bitmap bitmap = textAsBitmap(word);
            drawable = new BitmapDrawable(bitmap);
            drawable.setBounds(0,0,30*word.length(),80);
        } else {
            Pictogram pictogram = pictograms.get(position - words.size());
            int drawableId = findIdByString(pictogram.getUrl());
            drawable = ContextCompat.getDrawable(mContext, drawableId);
            drawable.setBounds(0,0,200, 200);
        }

        SpannableStringBuilder builder = new SpannableStringBuilder();

        ImageSpan img = new ImageSpan(drawable, "myImage");
        builder.append(" ");
        builder.setSpan(img,
                0, builder.length(), 0);
        holder.title.setText(builder);
        holder.current = position;

    }

    public static Bitmap textAsBitmap(String text) {
        Paint paint = new Paint();
        paint.setTextSize(24.0f);
        paint.setColor(Color.BLACK);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    public void addItem(int position) {
        mItems.add(position);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
