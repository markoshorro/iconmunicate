package se.chalmers.edu.iconmunicate;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import se.chalmers.edu.iconmunicate.controller.ConnectionManager;
import se.chalmers.edu.iconmunicate.controller.DatabaseManager;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.model.Message;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;
import se.chalmers.edu.iconmunicate.view.ItemGridAdapter;
import se.chalmers.edu.iconmunicate.view.SectionedGridRecyclerViewAdapter;

/**
 * Created by Markos Horro on 30/12/2015.
 */
public class GalleryActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    /**
     * To debug
     */
    private static final String TAG = "GalleryActivity";

    /**
     * Gson objects
     */
    private Gson gson = new Gson();

    /**
     * Recycler view
     */
    private RecyclerView mRecyclerView;
    private ItemGridAdapter mAdapter;

    private Conversation mReceiver;

    /**
     * Context
     */
    private Context mContext;
    private CoordinatorLayout mCoordinatorLayout;
    private Activity mActivity;

    private Snackbar mSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_gallery);

        mContext = getApplicationContext();
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        mActivity = this;

        Bundle extras = getIntent().getExtras();
        mReceiver = gson.fromJson(extras.getString(Constants.EXTRA_DEV_INFO),
                Conversation.class);

        //Your RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.grid_icons);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,4));

        //Your RecyclerView.Adapter
        mAdapter = new ItemGridAdapter(this);

        //This is the code to provide a sectioned grid
        SectionedGridRecyclerViewAdapter.Section[] sections;

        //Sections
        sections = new SectionedGridRecyclerViewAdapter.Section[]{
                    new SectionedGridRecyclerViewAdapter.Section(0, mContext.getString(R.string.section_comm)),
                    new SectionedGridRecyclerViewAdapter.Section(Constants.N_IC_COMM, mContext.getString(R.string.section_eatable)),
                    new SectionedGridRecyclerViewAdapter.Section(Constants.N_IC_COMM + Constants.N_IC_EAT, mContext.getString(R.string.section_location)),
                    new SectionedGridRecyclerViewAdapter.Section(Constants.N_IC_COMM + Constants.N_IC_EAT + Constants.N_IC_LOC,
                            mContext.getString(R.string.section_trans)),
                    new SectionedGridRecyclerViewAdapter.Section(Constants.N_IC_COMM + Constants.N_IC_EAT + Constants.N_IC_LOC +
                            Constants.N_IC_TRANS, mContext.getString(R.string.section_misc))};

        //Add your adapter to the sectionAdapter
        SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                SectionedGridRecyclerViewAdapter(this, R.layout.item_grid_header, R.id.item_grid_section_header,
                mRecyclerView,mAdapter);
        mSectionedAdapter.setSections(sections);
        //Apply this adapter to the RecyclerView
        mRecyclerView.setAdapter(mSectionedAdapter);

        final EditText inputText = (EditText) findViewById(R.id.conversation_input_text);

        // Listener for send button
        ImageButton sendBtn = (ImageButton) findViewById(R.id.conversation_btn_send_gallery);

        // Click on send message
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String visibility = Common.getValue(Constants.KEY_VISIBLE);

                // Test if connected
                if (TextUtils.equals(visibility, Constants.STATE_NOT_VISIBLE)) {
                    Toast.makeText(mContext, R.string.error_devices_dialog, Toast.LENGTH_SHORT).show();
                    return;
                }

                // Test if something to send
                Editable buffer = inputText.getText();
                if (buffer.length() == 0) {
                    Toast.makeText(mContext, R.string.nothing_to_send, Toast.LENGTH_SHORT).show();
                    return;
                }

                // Otherwise, send
                Log.d(TAG, "debugging: " + buffer.toString());
                Date dateMsg = new Date();

                // Publishing this one
                String deviceName = ConnectionManager.getInstance().getDeviceName();
                Log.d(TAG, "deviceName: " + deviceName);
                ArrayList<String> picsArray = correctExp(buffer.toString());
                Message messageToSave = new Message(picsArray, true, mReceiver.getId(),
                        mReceiver.getUsername(), dateMsg);
                Message messageToSend = new Message(picsArray, false, ConnectionManager.getInstance().getId(),
                        deviceName, dateMsg);
                String mMessageBody = gson.toJson(messageToSend);
                ConnectionManager.getInstance().publishMessage(mMessageBody, mReceiver.getId());

                // Save to database
                DatabaseManager.getInstance().createMessage(messageToSave);
                // Update interface
                Common.updateSharedPreference(Constants.KEY_UPDATE_CONVERSATION_LIST,
                        Constants.TASK_UPDATE);
                Common.updateSharedPreference(Constants.KEY_UPDATE_MESSAGE_LIST,
                        Constants.TASK_UPDATE);
                finish();
            }
        });

        mSnackbar = setUpSnackBar();
        Common.updateSharedPreference(Constants.KEY_INPUT_IC, Constants.TASK_NONE);
    }

    private Snackbar setUpSnackBar() {

        return Snackbar
                .make(mCoordinatorLayout, R.string.detele_ic, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.detele, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Get edit text and delete
                        EditText input = (EditText) (mActivity).findViewById(R.id.conversation_input_text);
                        StringBuffer newInput = new StringBuffer(input.getText().toString());
                        int index = newInput.substring(0, newInput.length() - 2).lastIndexOf(";");

                        SpannableStringBuilder builder = new SpannableStringBuilder();
                        builder.append(input.getText());
                        builder.delete(index + 1, newInput.length());

                        input.setText(builder);
                        if (newInput.length() == 0) {
                            mSnackbar.dismiss();
                            mSnackbar = setUpSnackBar();
                        }
                    }
                }).setCallback(new Snackbar.Callback() {
                                   @Override
                                   public void onDismissed(Snackbar snackbar, int event) {
                                       switch (event) {
                                           case Snackbar.Callback.DISMISS_EVENT_ACTION:
                                               mSnackbar = setUpSnackBar();
                                               updateSnack();
                                               break;
                                       }
                                   }
                               }
                ).setActionTextColor(Color.RED);
    }

    private ArrayList<String> correctExp(String buffer) {
        ArrayList<String> picsArray =  new ArrayList<>(Arrays.asList(buffer.split(";")));

        return picsArray;
    }

    /**
     * Managing errors
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }

    // Helper function
    public static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "connection to GoogleApiClient failed");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, final String key) {
        Log.d(TAG, "onSharedPreferenceChanged " + TAG + ": " + key);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.equals(key, Constants.KEY_INPUT_IC)) {
                    updateSnack();
                    return;
                }
            }
        });
    }

    private void updateSnack() {
        if (TextUtils.equals(Common.getValue(Constants.KEY_INPUT_IC), Constants.TASK_UPDATE)) {
            Log.d(TAG, "showing snack");
            mSnackbar.show();
            Common.updateSharedPreference(Constants.KEY_INPUT_IC, Constants.TASK_NONE);
            return;
        }
        EditText input = (EditText) (mActivity).findViewById(R.id.conversation_input_text);
        if (input.getText().length()>0) {
            mSnackbar.show();
        }
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();

        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");

        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
}
