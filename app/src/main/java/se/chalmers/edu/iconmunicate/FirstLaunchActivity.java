package se.chalmers.edu.iconmunicate;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import se.chalmers.edu.iconmunicate.controller.ConnectionManager;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by Markos Horro on 10/01/2016.
 */
public class FirstLaunchActivity extends Activity {

    private Button btnJoin;
    private EditText txtName;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_launch);

        btnJoin = (Button) findViewById(R.id.btnJoin);
        txtName = (EditText) findViewById(R.id.name);

        mContext = getApplicationContext();

        btnJoin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (txtName.getText().toString().trim().length() > 0) {
                    String name = txtName.getText().toString().trim();
                    PreferenceManager.getDefaultSharedPreferences(mContext)
                            .edit()
                            .putString(Constants.PREF_DEVICE_NAME, name)
                            .apply();
                    ConnectionManager.getInstance().updateDiscoverMsg();
                    PreferenceManager.getDefaultSharedPreferences(mContext)
                            .edit()
                            .putBoolean(Constants.PREF_FIRST_LAUNCH, Boolean.TRUE)
                            .apply();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),
                            R.string.error_insert_name, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),
                R.string.error_insert_name, Toast.LENGTH_LONG).show();
        return;
    }
}
