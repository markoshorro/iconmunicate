package se.chalmers.edu.iconmunicate.model;

import java.util.Date;

/**
 * Created by Markos Horro on 03/12/2015.
 */
public class Conversation {
    private String username;
    private String id;
    private Date lastMessage;
    private int newMsg;
    /**
     * Getters and setters
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Date lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void incrNewMsg() { newMsg++; }
    public void resetNewMsg() { newMsg = 0; }
    public int getNewMsg() { return newMsg; }

    /**
     * Constructors
     */
    public Conversation(String username, String id, Date lastMessage) {
        this.username = username;
        this.id = id;
        this.lastMessage = lastMessage;
        this.newMsg = 0;
    }
}
