package se.chalmers.edu.iconmunicate.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.model.Conversation;

/**
 * Created by Markos Horro on 26/12/2015.
 */
public class DevicesListAdapter extends BaseAdapter {

    private static Activity activity;
    private static ArrayList<Conversation> mDeviceList;
    private static LayoutInflater inflater;

    public DevicesListAdapter(Activity activity, ArrayList<Conversation> mDeviceList,
                              LayoutInflater inflater) {
        this.activity = activity;
        this.mDeviceList = mDeviceList;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return mDeviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDeviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if(convertView==null) {
            vi = inflater.inflate(R.layout.item_devices_list, null);
        }

        TextView device = (TextView)vi.findViewById(R.id.item_device_list);
        TextView subDevice = (TextView)vi.findViewById(R.id.item_sub_device_list);

        Conversation conversationItem = mDeviceList.get(position);

        device.setText(conversationItem.getUsername());
        subDevice.setText(conversationItem.getId());

        return vi;
    }

    /**
     * Helper function to update the list. S
     * @param mDeviceList
     */
    public void updateList(ArrayList<Conversation> mDeviceList){
        this.mDeviceList = mDeviceList;
    }
}
