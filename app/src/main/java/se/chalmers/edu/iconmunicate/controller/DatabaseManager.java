package se.chalmers.edu.iconmunicate.controller;

import android.content.Context;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.model.DatabaseHelper;
import se.chalmers.edu.iconmunicate.model.Message;
import se.chalmers.edu.iconmunicate.model.Pictogram;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by Markos Horro on 03/01/2016.
 */
public class DatabaseManager {
    /**
     * Singleton Pattern
     */
    private static final DatabaseManager mInstance = new DatabaseManager();
    public static DatabaseManager getInstance() { return mInstance; }

    private DatabaseHelper mDatabaseHelper;

    public void init(Context mContext) {
        mDatabaseHelper = new DatabaseHelper(mContext.getApplicationContext());
        //mDatabaseHelper.onCreate(mDatabaseHelper.getWritableDatabase());
    }

    public ArrayList<Conversation> getAllConversations(){
        return mDatabaseHelper.getAllConversations(mDatabaseHelper.getReadableDatabase());
    }

    public void updateConversation(Message newMessage){
        mDatabaseHelper.updateConversation(newMessage, mDatabaseHelper.getWritableDatabase());
    }

    public void deleteConversation(String conversation_id) {
        mDatabaseHelper.deleteConversation(conversation_id, mDatabaseHelper.getWritableDatabase());
    }

    public void createConversation(Conversation conversation) {
        mDatabaseHelper.createConversation(conversation, mDatabaseHelper.getWritableDatabase());
    }

    public ArrayList<Message> getMessagesOfConversation(String conversationId, String conversationUsername) {
        return mDatabaseHelper.getMessagesOfConversation(conversationId, conversationUsername, mDatabaseHelper.getReadableDatabase());
    }

    public long createMessage(Message message) {
        return mDatabaseHelper.createMessage(message, mDatabaseHelper.getWritableDatabase());
    }

    public ArrayList<Pictogram> getPictogramsByTag(String tag, Constants.Language language) {
        return mDatabaseHelper.getPictogramsByTag(tag, language, mDatabaseHelper.getReadableDatabase());
    }

    public String getPictogramDescription(long pictogram_id, Constants.Language language) {
        return mDatabaseHelper.getPictogramDescription(pictogram_id, language,
                mDatabaseHelper.getReadableDatabase());
    }

    public long setPictogramDescription(long pictogram_id, Constants.Language language, String description) {
        return mDatabaseHelper.setPictogramDescription(pictogram_id, language, description,
                mDatabaseHelper.getWritableDatabase());
    }

    public Message getLastMessageOfConversation(String conversationId) {
        return mDatabaseHelper.getLastMessageOfConversation(conversationId, mDatabaseHelper.getReadableDatabase());
    }

    public boolean conversationExists(String conversationId) {
        return mDatabaseHelper.conversationExists(conversationId, mDatabaseHelper.getReadableDatabase());
    }

    /**
     * For singleton pattern
     */
    public DatabaseManager() {
    }
}
