package se.chalmers.edu.iconmunicate.controller;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.preference.PreferenceManager;

import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by Markos Horro on 10/01/2016.
 */
public class NotificationsManager {
    /**
     * Singleton pattern
     */
    private static final NotificationsManager mInstance = new NotificationsManager();
    public static NotificationsManager getInstance() { return mInstance; }
    private NotificationsManager() {}

    /**
     * Context
     */
    private static Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public void newMessage() {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        String ringtone_uri = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.PREF_NOT_RINGTONE, Constants.DEFAULT_RING);
        Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(ringtone_uri));

        // If sound notifications enabled
        if (PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(Constants.PREF_NOT_NEW_MESSAGE, false)) {
            if (PreferenceManager.getDefaultSharedPreferences(context)
                    .getBoolean(Constants.PREF_NOT_VIBRATE, false)) {
                // Vibrate for 500 milliseconds
                v.vibrate(Constants.T_VIBRATE);
            }
            ringtone.play();
        }
    }
}
