package se.chalmers.edu.iconmunicate.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.controller.DatabaseManager;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.model.DevicesList;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by markoshorro on 13/12/15.
 */
public class DevicesListDialog extends DialogFragment implements
        AdapterView.OnItemClickListener {

    /**
     * Singleton pattern
     */
    private static final DevicesListDialog _instance = new DevicesListDialog();

    public static DevicesListDialog getInstance() { return _instance; }

    /**
     * Debug
     */
    private final static String TAG = "DevicesListFragment";

    /**
     * Devices list
     */
    private ListView mNearbyDevicesView;
    private ArrayList<Conversation> mNearbyDevicesArrayList = new ArrayList<>();
    private DevicesListAdapter mNearbyDevicesAdapter;

    /**
     * Creates the dialog and sets up the layout
     * @param savedInstanceState
     * @return dialog created
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Needed to inflate here the view instead onCreateView (requestfeature()
        // must be called before adding content)
        View view = getActivity().getLayoutInflater().
                inflate(R.layout.fragment_list_devices, null, false);
        mNearbyDevicesView = (ListView) view.findViewById(R.id.nearby_devices_list);

        mNearbyDevicesAdapter = new DevicesListAdapter(getActivity(),
                DevicesList.getDevicesList().getAllDevices(), getActivity().getLayoutInflater());

        mNearbyDevicesView.setAdapter(mNearbyDevicesAdapter);
        mNearbyDevicesView.setOnItemClickListener(this);
        return new AlertDialog.Builder(getActivity())
                .setNegativeButton(R.string.button_dialog_close,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setTitle(R.string.dialog_title_devices)
                .setView(view)
                .create();
    }

    /**
     * To update list from Main Activity
     */
    public void updateList() {
        Log.d(TAG, "updateList");
        if ((mNearbyDevicesArrayList!=null)&&(mNearbyDevicesAdapter!=null)) {
            mNearbyDevicesAdapter.updateList(DevicesList.getDevicesList().getAllDevices());
            mNearbyDevicesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Conversation device = (Conversation) mNearbyDevicesAdapter.getItem(position);
        DatabaseManager.getInstance().createConversation(device);
        dismiss();
        // Updating shared preferences triggers the listener on main activity
        Common.updateSharedPreference(
                Constants.KEY_UPDATE_CONVERSATION_LIST, Constants.TASK_UPDATE);
    }
}
