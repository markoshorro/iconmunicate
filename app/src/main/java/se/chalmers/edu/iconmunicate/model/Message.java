package se.chalmers.edu.iconmunicate.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Markos Horro on 03/12/2015.
 */
public class Message {

    private long id;
    private ArrayList<String> pictograms;
    private boolean userIsSender; //true if message sent, false is message received
    private String conversationId; //user id of the other person
    private String conversationUsername; //user id of the other person
    private Date date;

    public static final String PICTOGRAM_FORMAT = "pict";
    public static final String TEXT_FORMAT = "text";
    public static final String FORMAT_SEPARATOR = ":";
    public static final String SERIAL_SEPARATOR = " ";
    public static final int IS_TEXT = 0;
    public static final int IS_PICTOGRAM = 1;

    public Message(ArrayList<String> pictograms,
                   boolean userIsSender, String conversationId, String conversationUsername, Date date) {
        this.pictograms = pictograms;
        this.userIsSender = userIsSender;
        this.conversationId = conversationId;
        this.conversationUsername = conversationUsername;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<String> getPictograms() {
        return pictograms;
    }

    public void setPictograms(ArrayList<String> pictograms) {
        this.pictograms = pictograms;
    }

    public boolean getUserIsSender() {
        return userIsSender;
    }

    public void setUserIsSender(boolean userIsSender) {
        this.userIsSender = userIsSender;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getConversationUsername() {
        return conversationUsername;
    }

    public void setConversationUsername(String conversationUsername) {
        this.conversationUsername = conversationUsername;
    }

    public String serializePictograms(){
        String serialized = "";
        if(pictograms.size() > 0)
            serialized = pictograms.get(0);
        for(int i = 1; i < pictograms.size(); i++) {
            serialized += SERIAL_SEPARATOR;
            serialized += pictograms.get(i);
        }
        return serialized;
    }

    public static final ArrayList<String> deserializePictograms(String serialized){
        ArrayList<String> deserialized = new ArrayList<>();
        String[] parts = serialized.split(SERIAL_SEPARATOR);
        for(String pictogram: parts){
            deserialized.add(pictogram);
        }
        return deserialized;
    }

    public static final int pictogramIsText(String pictogram){
        String[] parts = pictogram.split(FORMAT_SEPARATOR);
        if(parts != null){
            if(parts[0].equals(TEXT_FORMAT))
                return IS_TEXT;
            if (parts[0].equals(PICTOGRAM_FORMAT))
                return IS_PICTOGRAM;
        }
        return -1;
    }

    public static final long extractPictogramId(String pictogram){
        String[] parts = pictogram.split(FORMAT_SEPARATOR);
        if (parts[0].equals(PICTOGRAM_FORMAT) && parts[1] != null)
            return Long.parseLong(parts[1]);
        return -1;
    }

    public static final String extractPictogramText(String pictogram){
        String[] parts = pictogram.split(FORMAT_SEPARATOR);
        if (parts[0].equals(TEXT_FORMAT) && parts[1] != null)
            return parts[1];
        return "";
    }

}
