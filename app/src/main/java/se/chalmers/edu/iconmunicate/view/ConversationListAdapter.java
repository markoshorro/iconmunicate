package se.chalmers.edu.iconmunicate.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.util.Common;

/**
 * Created by markoshorro on 14/12/15.
 */
public class ConversationListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Conversation> conversationList;
    private static LayoutInflater inflater;

    public ConversationListAdapter(Activity activity, ArrayList<Conversation> conversationList,
                                   LayoutInflater inflater) {
        this.activity = activity;
        this.conversationList = conversationList;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return conversationList.size();
    }

    @Override
    public Object getItem(int position) {
        return conversationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if(convertView==null) {
            vi = inflater.inflate(R.layout.item_conversation_list, null);
        }
        vi.setBackgroundResource(R.drawable.item_conversation_list_bg);

        TextView device = (TextView)vi.findViewById(R.id.item_conversation_device);
        TextView subDevice = (TextView)vi.findViewById(R.id.item_conversation_subdevice);
        TextView date = (TextView) vi.findViewById(R.id.item_conversation_date);
        TextView newMessage = (TextView) vi.findViewById(R.id.item_conversation_msg_new);

        Conversation conversationItem = conversationList.get(position);


        int newMsgs = Common.getNewMessages(conversationItem.getId());

        if (newMsgs > 0) {
            newMessage.setVisibility(View.VISIBLE);
            newMessage.setText(String.valueOf(newMsgs));
        } else {
            newMessage.setVisibility(View.GONE);
        }

        device.setText(conversationItem.getUsername());
        subDevice.setText(conversationItem.getId());

        // This is why I hate to work with dates in Java...
        String dateString = Common.dateToString(vi, conversationItem.getLastMessage());
        date.setText(dateString);

        return vi;
    }

    /**
     * Helper funciton to update database
     * @param allConversations
     */
    public void updateList(ArrayList<Conversation> allConversations) {
        this.conversationList = allConversations;
        notifyDataSetChanged();
    }
}
