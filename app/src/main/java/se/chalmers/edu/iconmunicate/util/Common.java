package se.chalmers.edu.iconmunicate.util;

import android.content.Context;
import android.view.View;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import se.chalmers.edu.iconmunicate.R;

/**
 * Created by Markos Horro on 09/12/2015.
 *
 * This is a helper class that contains methods to simplify some tasks as managing
 * SharePreferences
 *
 */
public class Common {
    /**
     * Singleton pattern
     */
    private static final Common mInstance = new Common();
    public static Common getInstance() {
        return mInstance;
    }
    private static Context mContext;
    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Returns a String value given a key
     * @param key
     * @return
     */
    public static String getValue(String key) {
        return mContext.getSharedPreferences(Constants.SHARED_PREF_NAME,
                Context.MODE_PRIVATE).getString(key, Constants.TASK_NONE);
    }

    public static String getValue(String key, String defaultValue) {
        return mContext.getSharedPreferences(Constants.SHARED_PREF_NAME,
                Context.MODE_PRIVATE).getString(key, defaultValue);
    }

    /**
     * Helper for editing entries in SharedPreferences.
     */
    public static void updateSharedPreference(String key, String value) {
        mContext.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    public static void increaseNewMessage(String conversationId) {
        int newValue = mContext.getSharedPreferences(Constants.SHARED_PREF_NAME_MSG, Context.MODE_PRIVATE)
                .getInt(conversationId, 0);
        mContext.getSharedPreferences(Constants.SHARED_PREF_NAME_MSG, Context.MODE_PRIVATE)
                .edit()
                .putInt(conversationId, newValue + 1)
                .apply();
    }

    public static void resetNewMessages(String conversationId) {
        mContext.getSharedPreferences(Constants.SHARED_PREF_NAME_MSG, Context.MODE_PRIVATE)
                .edit()
                .putInt(conversationId, 0)
                .apply();
    }

    public static int getNewMessages(String conversationId) {
        return mContext.getSharedPreferences(Constants.SHARED_PREF_NAME_MSG, Context.MODE_PRIVATE)
                .getInt(conversationId, 0);
    }

    /**
     * Resets the state of pending subscription and publication tasks.
     */
    public static void resetToDefaultState() {
        mContext.getSharedPreferences(Constants.SHARED_PREF_NAME,Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.KEY_VISIBLE, Constants.STATE_NOT_VISIBLE)
                .apply();
    }

    public static String dateToString(View vi, Date date) {
        // This is why I hate to work with dates in Java...
        SimpleDateFormat dateFormatterDay = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateFormatterHour = new SimpleDateFormat("HH:mm");
        // Just in case...
        String dateString = "unknown";
        try {
            Date today = dateFormatterDay.parse(dateFormatterDay.format(new Date()));
            // Default (if not today or yesterday)
            Date dateObjDay = dateFormatterDay.parse(dateFormatterDay.format(date));
            dateString = dateFormatterDay.format(date);
            // Today
            if (today.equals(dateObjDay)) {
                dateString = dateFormatterHour.format(date);
            }
            // Yesterday
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            Date yesterday = cal.getTime();
            yesterday = dateFormatterDay.parse(dateFormatterDay.format(yesterday));
            if (yesterday.equals(dateObjDay)) {
                dateString = vi.getResources().getString(R.string.item_conversation_yesterday);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }
}
