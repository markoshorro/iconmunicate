package se.chalmers.edu.iconmunicate.controller;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeCallback;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.model.DeviceMessage;
import se.chalmers.edu.iconmunicate.model.DevicesList;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;
import se.chalmers.edu.iconmunicate.view.DevicesListDialog;

/**
 * Created by Markos Horro on 27/12/2015.
 */
public class ConnectionManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /**
     * TAG for debugging
     */
    private static final String TAG = "ConnectionManager";

    /**
     * Sets the time in seconds for a published message or a subscription to live. Set to three
     * minutes.
     */
    private final static Strategy PUB_SUB_STRATEGY_DISCOVER = new Strategy.Builder()
            .setTtlSeconds(Constants.TTL_SEC_DISCOVER).build();

    private static Strategy PUB_SUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(Constants.TTL_SEC_MSG).build();
    /**
     * Singleton pattern
     */
    private static final ConnectionManager _instance = new ConnectionManager();

    static public ConnectionManager getInstance(){
        return _instance;
    }

    /**
     * Device list dialog. It is handled with fragManager
     */
    private DevicesListDialog devicesDialog;

    private boolean mResolvingNearbyPermissionError;

    /**
     * Unique identifier
     */
    private String id;

    /**
     * A {@link MessageListener} for processing messages from nearby devices.
     */
    private MessageListener mMessageListener;

    /**
     * Google Api Client
     */
    private GoogleApiClient mGoogleApiClient;

    private Message mDeviceInfoMessage;

    private Message mDiscoverMessage;

    /**
     * To clean all the messages
     */
    private ArrayList<Message> mDeviceInfoMessageList = new ArrayList<>();

    /**
     * Context
     */
    private Context context;
    private Activity activity;
    private Gson gson = new Gson();

    public void init(Context context) {
        this.context = context;

        devicesDialog = DevicesListDialog.getInstance();

        String defaultName = Build.MODEL;
        String deviceName = PreferenceManager.getDefaultSharedPreferences(activity)
                .getString(Constants.PREF_DEVICE_NAME, defaultName);
        mDiscoverMessage = DeviceMessage.newNearbyMessage(
                id,
                deviceName,
                true);

        mMessageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                Log.d(TAG, "onFound");
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DeviceMessage deviceMessage = DeviceMessage.fromNearbyMessage(message);

                        if (deviceMessage.isDiscoverMsg()) {
                            Conversation device = new Conversation(deviceMessage.getMessageBody(),
                                    deviceMessage.getmInstanceId(), new Date());
                            DevicesList.getDevicesList().addDevice(device);
                            devicesDialog.updateList();
                        } else {
                            parseMessage(deviceMessage);
                        }
                    }
                });
            }

            @Override
            public void onLost(final Message message) {
                // Called when a message is no longer detectable nearby.
                Log.d(TAG, "onLost");
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DevicesList.getDevicesList().removeDevice(
                                DeviceMessage.fromNearbyMessage(message).getmInstanceId());
                        devicesDialog.updateList();
                    }
                });
            }
        };

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Update Discover Message
     */
    public void updateDiscoverMsg() {
        String visibility = Common.getValue(Constants.KEY_VISIBLE);

        // Publishing this one
        String deviceName = getDeviceName();
        mDiscoverMessage = DeviceMessage.newNearbyMessage(
                id, deviceName, true);

        if (TextUtils.equals(visibility, Constants.STATE_VISIBLE)) {
            unpublishDiscover();
            publishDiscover();
        }
    }

    public String getDeviceName() {
        String defaultName = Build.MODEL;
        String deviceName = PreferenceManager.getDefaultSharedPreferences(activity)
                .getString(Constants.PREF_DEVICE_NAME, defaultName);
        return deviceName;
    }

    /**
     * Publishing a custom message to a device
     */
    public void publishMessage(String mMessageBody, String mReceiver) {
        mDeviceInfoMessage = DeviceMessage.newNearbyMessage(
                id, mMessageBody, mReceiver);
        mDeviceInfoMessageList.add(mDeviceInfoMessage);
        publish();
    }

    /**
     * Unpublish all messages
     */
    public void unpublishAll() {
        for (int i = 0; i<mDeviceInfoMessageList.size(); i++) {
            Log.d(TAG, "unpublish all");
            mDeviceInfoMessage = mDeviceInfoMessageList.get(i);
            unpublish();
        }
        unpublishDiscover();
        mDeviceInfoMessageList = new ArrayList<>();
    }

    /**
     * Connects Google Api Client
     */
    public void connect() {
        mGoogleApiClient.connect();
    }

    /**
     * Disconnects Google Api Client
     */
    public void disconnect() {
        mGoogleApiClient.disconnect();
    }

    /**
     * Check if it is connected
     */
    public boolean isConnected() {
        return mGoogleApiClient.isConnected();
    }

    /**
     * Clears items from the adapter
     */
    private void clearDeviceList() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DevicesList.getDevicesList().clearList();
            }
        });
    }


    private void parseMessage(DeviceMessage deviceMessage) {
        Log.d(TAG, "parsing message...");
        String receiver = deviceMessage.getmReceiver();
        if (TextUtils.equals(receiver, id)) {

            Log.d(TAG, "message to me: " + deviceMessage.getMessageBody());
            Log.d(TAG, "message from: " + deviceMessage.getmInstanceId());

            String from = deviceMessage.getmInstanceId();
            se.chalmers.edu.iconmunicate.model.Message newMessage =
                    gson.fromJson(deviceMessage.getMessageBody(),
                            se.chalmers.edu.iconmunicate.model.Message.class);
            if (DatabaseManager.getInstance().conversationExists(from)) {
                se.chalmers.edu.iconmunicate.model.Message lastMessage =
                        DatabaseManager.getInstance().getLastMessageOfConversation(from);
                if (lastMessage!=null) {
                    if ((lastMessage.getDate().equals(newMessage.getDate())) ||
                            (lastMessage.getDate().after(newMessage.getDate()))) {
                        Log.d(TAG, "parsing the same message received");
                        return;
                    }
                }
            }
            // Even if the conversation does not exist, it will be created
            NotificationsManager.getInstance().newMessage();
            DatabaseManager.getInstance().createMessage(newMessage);
            Common.increaseNewMessage(from);
            Common.updateSharedPreference(Constants.KEY_UPDATE_MESSAGE_LIST, Constants.TASK_UPDATE);
            Common.updateSharedPreference(Constants.KEY_UPDATE_CONVERSATION_LIST, Constants.TASK_UPDATE);
        }
    }

    /**
     * Subscribes to messages from nearby devices. If not successful, attempts to resolve any error
     * related to Nearby permissions by displaying an opt-in dialog. Registers a callback which
     * updates state when the subscription expires.
     */
    public void subscribe() {
        Log.i(TAG, "trying to subscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            clearDeviceList();
            SubscribeOptions options = new SubscribeOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY_DISCOVER)
                    .setCallback(new SubscribeCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer subscribing");
                            if (devicesDialog != null) {
                                devicesDialog.dismiss();
                            }
                        }
                    }).build();

            Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "subscribed successfully");
                            } else {
                                Log.i(TAG, "could not subscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    /**
     * Ends the subscription to messages from nearby devices. If successful, resets state. If not
     * successful, attempts to resolve any error related to Nearby permissions by
     * displaying an opt-in dialog.
     */
    public void unsubscribe() {
        Log.i(TAG, "trying to unsubscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unsubscribed successfully");
                                Common.updateSharedPreference(
                                        Constants.KEY_VISIBLE,
                                        Constants.STATE_NOT_VISIBLE);
                            } else {
                                Log.i(TAG, "could not unsubscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    /**
     * Publishes device information to nearby devices. If not successful, attempts to resolve any
     * error related to Nearby permissions by displaying an opt-in dialog. Registers a callback
     * that updates the UI when the publication expires.
     */
    public void publish() {
        Log.i(TAG, "trying to publish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            PublishOptions options = new PublishOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new PublishCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer publishing");
                        }
                    }).build();

            Nearby.Messages.publish(mGoogleApiClient, mDeviceInfoMessage, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "published successfully");
                            } else {
                                Log.i(TAG, "could not publish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    public void publishDiscover() {
        Log.i(TAG, "trying to publish discover message");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            PublishOptions options = new PublishOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY_DISCOVER)
                    .setCallback(new PublishCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer publishDiscover");
                            Common.updateSharedPreference(
                                    Constants.KEY_VISIBLE,
                                    Constants.STATE_NOT_VISIBLE);
                        }
                    }).build();

            Nearby.Messages.publish(mGoogleApiClient, mDiscoverMessage, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "publishDiscover successfully");
                            } else {
                                Log.i(TAG, "could not publishDiscover");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    /**
     * Stops publishing device information to nearby devices. If successful, resets state. If not
     * successful, attempts to resolve any error related to Nearby permissions by displaying an
     * opt-in dialog.
     */
    public void unpublish() {
        Log.i(TAG, "trying to discover unpublish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unpublish(mGoogleApiClient, mDeviceInfoMessage)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unpublished successfully");
                            } else {
                                Log.i(TAG, "could not unpublish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    public void unpublishDiscover() {
        Log.i(TAG, "trying to unpublish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unpublish(mGoogleApiClient, mDiscoverMessage)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unpublishDiscover successfully discover");
                            } else {
                                Log.i(TAG, "could not unpublishDiscover");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    /**
     * Managing errors
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }

    // Helper function
    public static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "connection to GoogleApiClient failed");
    }

    /**
     * Handles errors generated when performing a subscription or publication action. Uses
     * {@link Status#startResolutionForResult} to display an opt-in dialog to handle the case
     * where a device is not opted into using Nearby.
     */
    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i(TAG, "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!mResolvingNearbyPermissionError) {
                try {
                    mResolvingNearbyPermissionError = true;
                    status.startResolutionForResult(activity,
                            Constants.REQUEST_RESOLVE_ERROR);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (status.getStatusCode() == ConnectionResult.NETWORK_ERROR) {
                Toast.makeText(activity.getApplicationContext(),
                        R.string.error_connection_settings,
                        Toast.LENGTH_SHORT).show();
                Common.resetToDefaultState();
            } else {
                // To keep things simple, pop a toast for all other error messages.
                Toast.makeText(activity.getApplicationContext(), "Unsuccessful: " +
                        status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }

        }
        Common.updateSharedPreference(Constants.KEY_VISIBLE, Constants.STATE_NOT_VISIBLE);
    }

    /**
     * Setters for private attributes
     */
    public void setId(String id) {
        this.id = id;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    public String getId() { return id; }

    /**
     * Private constructor
     */
    private ConnectionManager() {}
}
