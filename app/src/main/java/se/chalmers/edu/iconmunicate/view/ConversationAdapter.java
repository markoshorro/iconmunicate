package se.chalmers.edu.iconmunicate.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.model.Message;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by markoshorro on 14/12/15.
 */
public class ConversationAdapter extends BaseAdapter {

    private static final String TAG = "ConversationAdapter";

    private static Activity activity;
    private ArrayList<Message> conversationList;

    public ConversationAdapter(Activity activity, ArrayList<Message> conversationList) {
        this.activity = activity;
        this.conversationList = conversationList;
    }

    @Override
    public int getCount() {
        return conversationList.size();
    }

    @Override
    public Object getItem(int position) {
        return conversationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View vi, ViewGroup parent) {

        Message message = conversationList.get(position);

        LayoutInflater mInflater = (LayoutInflater) activity
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (message.getUserIsSender()) {
            vi = mInflater.inflate(R.layout.item_conversation_msg_right, null);
        } else {
            vi = mInflater.inflate(R.layout.item_conversation_msg_left, null);
        }

        TextView msg = (TextView) vi.findViewById(R.id.item_conversation_msg_text);
        TextView date = (TextView) vi.findViewById(R.id.item_conversation_msg_date);
        String dateString = Common.dateToString(vi, message.getDate());

        SpannableStringBuilder builder = buildSpannable(Constants.SIZE_IC_MSG,
                message.getPictograms());

        // Fields of item
        msg.setText(builder);
        date.setText(dateString);

        return vi;
    }

    private SpannableStringBuilder buildSpannable(int size, ArrayList<String> message) {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        for (int i = 0; i<message.size(); i++) {
            String pic = message.get(i);
            if (pic.contains("pic")) {
                String iconId = pic.substring(pic.indexOf(":") + 1, pic.indexOf("]"));
                int drawableId = findIdByString(Constants.IC_SET + iconId);
                Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
                drawable.setBounds(0, 0, size, size);
                ImageSpan imageSpan = new ImageSpan(drawable);

                builder.append(message.get(i));
                builder.setSpan(imageSpan,
                        builder.length() - message.get(i).length(), builder.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (pic.contains("text")) {
                builder.append(" ");
                builder.append(pic.substring(pic.indexOf(":") + 1, pic.length()-1));
                builder.append(" ");
            }
        }

        return builder;
    }

    private static int findIdByString(String string) {
        String packageName = activity.getPackageName();
        int drawableId = activity.getResources().getIdentifier(string,
                "drawable", packageName);
        return drawableId;
    }


    /**
     * Helper funciton to update database
     * @param allMessages
     */
    public void updateList(ArrayList<Message> allMessages) {
        this.conversationList.clear();
        this.conversationList.addAll(allMessages);
        this.notifyDataSetChanged();
    }
}
