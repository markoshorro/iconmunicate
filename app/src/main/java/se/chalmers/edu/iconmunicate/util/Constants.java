package se.chalmers.edu.iconmunicate.util;

/**
 * iConmunicate constants used along the project
 */

public class Constants {
    /**
     * Request code to use when launching the resolution activity.
     */
    public static final int REQUEST_RESOLVE_ERROR = 1001;

    public static final int TTL_SEC_DISCOVER = 10 * 60;
    public static final int TTL_SEC_MSG      = 10 * 60;

    public static final String SHARED_PREF_NAME = "shared_preferences_application";
    public static final String SHARED_PREF_NAME_MSG = "shared_preferences_application_msg";

    // Keys to get and set the current subscription and publication tasks using SharedPreferences.
    public static final String KEY_UPDATE_CONVERSATION_LIST = "update_conv_list";
    public static final String KEY_ACT_CONVERSATION = "act_conversation";
    public static final String KEY_UPDATE_MESSAGE_LIST = "update_msg_list";
    public static final String KEY_INPUT_IC = "new_ic_in_input";
    public static final String KEY_VISIBLE = "visibility";

    // State
    public static final String STATE_VISIBLE = "visible";
    public static final String STATE_NOT_VISIBLE = "not_visible";

    // Tasks constants.
    public static final String TASK_NONE = "task_none";
    public static final String TASK_UPDATE = "task_update";

    // Preferences
    public static final String PREF_DEVICE_NAME = "pref_device_name";
    public static final String PREF_NOT_VIBRATE = "notifications_new_message_vibrate";
    public static final String PREF_NOT_RINGTONE = "notifications_new_message_ringtone";
    public static final String PREF_NOT_NEW_MESSAGE = "notifications_new_message";

    // For activities
    public static final String ACTIVITY_RUN = "act_run";

    // Extra values on intents
    public static final String EXTRA_DEV_INFO = "dev_info";

    // Constants for icons
    public static final String IC_SET  = "ic_set_";

    // Relative to icons
    public static final int N_IC_COMM   = 9;
    public static final int N_IC_EAT    = 3;
    public static final int N_IC_LOC    = 8;
    public static final int N_ICS       = 32;
    public static final int N_IC_TRANS  = 5;

    public static final int MAX_IC_MSG = 5;
    public static final int SIZE_IC_MSG = 108;

    // Relative to notifications
    public static final long T_VIBRATE = 500;
    public static final String DEFAULT_RING = "content://settings/system/notification_sound";

    // For first launch
    public static final String PREF_FIRST_LAUNCH = "first_launch";

    public enum Language {
        ENGLISH,
        FRENCH,
        SPANISH,
        GALICIAN
    }

}