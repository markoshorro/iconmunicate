package se.chalmers.edu.iconmunicate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.ArrayList;

import se.chalmers.edu.iconmunicate.controller.ConnectionManager;
import se.chalmers.edu.iconmunicate.controller.DatabaseManager;
import se.chalmers.edu.iconmunicate.model.Conversation;
import se.chalmers.edu.iconmunicate.model.Message;
import se.chalmers.edu.iconmunicate.util.Common;
import se.chalmers.edu.iconmunicate.util.Constants;
import se.chalmers.edu.iconmunicate.view.ConversationAdapter;

/**
 * Created by markoshorro on 14/12/15.
 */
public class ConversationActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener  {

    /**
     * Debugging zone
     */
    private static final String TAG = "ConversationActivity";

    /**
     * Device Info
     */
    private Conversation mDevice;

    /**
     * Helper Gson parser
     */
    private static final Gson gson = new Gson();

    /**
     * Conversation view
     */
    private ListView mConversationView;
    private ArrayList<Message> mConversationList = new ArrayList<>();
    private ConversationAdapter mConversationAdapter;

    private EditText editText;

    private ImageButton sendButton;
    private boolean textChanged = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);

        // Retrieving information from the parent activity
        if (savedInstanceState != null) {
            Log.d(TAG, "savedInstanceState");
            // Restore value of members from saved state
            mDevice = gson.fromJson(savedInstanceState.getString(Constants.EXTRA_DEV_INFO),
                    Conversation.class);
        } else {
            Log.d(TAG, "savedInstanceState null");
            Bundle extras = getIntent().getExtras();
            if (extras!=null) {
                mDevice = gson.fromJson(extras.getString(Constants.EXTRA_DEV_INFO),
                        Conversation.class);
                Log.d(TAG, "getIntent");
            }
        }

        // Setting view and initializing
        setContentView(R.layout.activity_conversation);
        ConnectionManager.getInstance().setActivity(this);

        // Setting the view
        setTitle(mDevice.getUsername());

        // Setting listener on button
        ImageButton imgButt = (ImageButton) findViewById(R.id.conversation_btn_categories);
        imgButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(_openGallery());
                    }
                }).start();
            }
        });

        // Setting button
        sendButton = (ImageButton) findViewById(R.id.conversation_btn_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(_openSearch());
                    }
                }).start();
            }
        });

        // Setting listener on input
        editText = (EditText) findViewById(R.id.conversation_input_text);
        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(_openSearch());
                        }
                    }).start();
                    return true;
                }
                return false;
            }
        });
        editText.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                if ((s.length()==1)&&(textChanged)) {
                    AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(300);
                    sendButton.startAnimation(anim);
                    sendButton.setVisibility(View.VISIBLE);
                    textChanged = false;
                }
                if (s.length()==0) {
                    AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
                    anim.setDuration(300);
                    sendButton.startAnimation(anim);
                    sendButton.setVisibility(View.GONE);
                    textChanged = true;
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        // Setting action bar
        setupActionBar();

        // Setting list
        mConversationView = (ListView) findViewById(R.id.conversation_message_list);
        mConversationList = DatabaseManager.getInstance()
                .getMessagesOfConversation(mDevice.getId(), mDevice.getUsername());
        mConversationAdapter = new ConversationAdapter(this, mConversationList);
        mConversationView.setAdapter(mConversationAdapter);

        // Registering activity
        getPreferences(Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * Private method to open the Conversation Activity
     */
    private Runnable _openGallery() {
        try {
            final Intent intent = new Intent(this, GalleryActivity.class);
            return new Runnable() {
                @Override
                public void run() {
                    intent.putExtra(Constants.EXTRA_DEV_INFO, gson.toJson(mDevice));
                    startActivity(intent);
                }
            };
        } catch (final Exception e) {
            e.printStackTrace();
            return new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),
                            R.string.error_unexpected, Toast.LENGTH_LONG).show();
                }
            };
        }
    }

    private Runnable _openSearch() {
        try {
            final Intent intent = new Intent(this, SearchActivity.class);
            return new Runnable() {
                @Override
                public void run() {
                    intent.putExtra("input", editText.getText().toString());
                    intent.putExtra(Constants.EXTRA_DEV_INFO, gson.toJson(mDevice));
                    startActivity(intent);
                }
            };
        } catch (final Exception e) {
            e.printStackTrace();
            return new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),
                            R.string.error_unexpected, Toast.LENGTH_LONG).show();
                }
            };
        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    // Needed for up navigation
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onSaveInstanceState");
        // Save the user's current game state
        savedInstanceState.putString(Constants.EXTRA_DEV_INFO, gson.toJson(mDevice));

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");

        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore state members from saved instance
        mDevice = gson.fromJson(savedInstanceState.getString(Constants.EXTRA_DEV_INFO),
                Conversation.class);
    }

    /**
     * Managing errors
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }

    // Helper function
    public static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "connection to GoogleApiClient failed");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, final String key) {
        Log.d(TAG, "onSharedPreferenceChanged " + TAG + ": " + key);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.equals(key, Constants.KEY_UPDATE_MESSAGE_LIST)) {
                    updateUI();
                }
            }
        });
    }

    /**
     * Private method to update the message list according to receive or sent messages
     */
    private void updateUI() {
        if (mConversationAdapter != null) {
            mConversationAdapter.updateList(DatabaseManager.getInstance()
                    .getMessagesOfConversation(mDevice.getId(), mDevice.getUsername()));
            // Update shared preferences
            Common.updateSharedPreference(Constants.KEY_UPDATE_MESSAGE_LIST,
                    Constants.TASK_NONE);
        }
    }

    /**
     * Lifecycle
     */
    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();

        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        // Saying that all message are read
        Common.resetNewMessages(mDevice.getId());
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();

        getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        // Updating the interface
        updateUI();
    }
}
