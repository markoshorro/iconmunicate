package se.chalmers.edu.iconmunicate.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import se.chalmers.edu.iconmunicate.R;
import se.chalmers.edu.iconmunicate.util.Constants;
import se.chalmers.edu.iconmunicate.util.XMLParser;

/**
 * Created by Inès on 09-Dec-15.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    Context context;

    // Logcat tag
    private static final String LOG = DatabaseHelper.class.getName();

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "iConmunicateDB";

    // Pictogram init file path
    //private static final String PICTOGRAM_INIT_PATH = "/app/src/main/res/values/pictograms.xml";
    private static final String PICTOGRAM_INIT_PATH = "values/pictograms.xml";

    // Table Names
    private static final String TABLE_PICTOGRAMS = "pictograms";
    private static final String TABLE_PICTOGRAM_TAGS = "pictogram_tags";
    private static final String TABLE_PICTOGRAM_DESCRIPTIONS = "pictogram_descriptions";
    private static final String TABLE_MESSAGES = "messages";
    private static final String TABLE_CONVERSATIONS = "conversations";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_PICTOGRAM_ID = "pictogram_id";
    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_CONVERSATION_ID = "conversation_id";

    // PICTOGRAMS Table - column names
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_URL = "url";

    // PICTOGRAM_TAGS Table - column names
    private static final String KEY_TAG = "tag";

    // PICTOGRAM_DESCRIPTION Table - column names
    private static final String KEY_DESCRIPTION = "description";

    // MESSAGES Table - column names
    private static final String KEY_PICTOGRAMS = "pictograms";
    private static final String KEY_SENDER = "sender";
    private static final String KEY_DATE = "date";

    // CONVERSATIONS Table - column names
    private static final String KEY_USERNAME = "username";
    private static final String KEY_LAST_MESSAGE = "last_message";

    private static final int TRUE = 1;
    private static final int FALSE = 0;

    // Table Create Statements
    private static final String CREATE_TABLE_PICTOGRAMS = "CREATE TABLE " + TABLE_PICTOGRAMS
            + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_CATEGORY + " TEXT,"
            + KEY_URL + " TEXT);";

    private static final String CREATE_TABLE_PICTOGRAM_TAGS = "CREATE TABLE " + TABLE_PICTOGRAM_TAGS
            + "(" + KEY_PICTOGRAM_ID + " INTEGER,"
            + KEY_LANGUAGE + " TEXT,"
            + KEY_TAG + " TEXT,"
            + "FOREIGN KEY (" + KEY_PICTOGRAM_ID + ") REFERENCES " + TABLE_PICTOGRAMS + "(" + KEY_ID + "));";

    private static final String CREATE_TABLE_PICTOGRAM_DESCRIPTIONS = "CREATE TABLE " + TABLE_PICTOGRAM_DESCRIPTIONS
            + "(" + KEY_PICTOGRAM_ID + " INTEGER,"
            + KEY_LANGUAGE + " TEXT,"
            + KEY_DESCRIPTION + " TEXT,"
            + "PRIMARY KEY (" + KEY_PICTOGRAM_ID + "," + KEY_LANGUAGE + "),"
            + "FOREIGN KEY (" + KEY_PICTOGRAM_ID + ") REFERENCES " + TABLE_PICTOGRAMS + "(" + KEY_ID + "));";

    private static final String CREATE_TABLE_MESSAGES = "CREATE TABLE " + TABLE_MESSAGES
            + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_PICTOGRAMS + " TEXT,"
            + KEY_SENDER + " INTEGER,"
            + KEY_CONVERSATION_ID + " TEXT,"
            + KEY_DATE + " DATETIME,"
            + "FOREIGN KEY (" + KEY_CONVERSATION_ID + ") REFERENCES " + TABLE_CONVERSATIONS + "(" + KEY_CONVERSATION_ID + "));";

    private static final String CREATE_TABLE_CONVERSATIONS = "CREATE TABLE " + TABLE_CONVERSATIONS
            + "(" + KEY_CONVERSATION_ID + " TEXT PRIMARY KEY,"
            + KEY_USERNAME + " TEXT,"
            + KEY_LAST_MESSAGE + " DATETIME" + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_PICTOGRAMS);
        db.execSQL(CREATE_TABLE_PICTOGRAM_TAGS);
        db.execSQL(CREATE_TABLE_PICTOGRAM_DESCRIPTIONS);
        db.execSQL(CREATE_TABLE_MESSAGES);
        db.execSQL(CREATE_TABLE_CONVERSATIONS);

        // filling the pictogram table
        fillPictogramTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICTOGRAMS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICTOGRAM_TAGS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICTOGRAM_DESCRIPTIONS + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONVERSATIONS + ";");

        // create new tables
        onCreate(db);
    }

    // ------------------------ "pictograms" table methods ----------------//

    //filling the table with preset pictograms
    private void fillPictogramTable(SQLiteDatabase db) {
        XMLParser xmlParser = new XMLParser();
        ArrayList<Pictogram> pictograms = new ArrayList<Pictogram>();
        try {
            XmlResourceParser xpp = context.getResources().getXml(R.xml.pictograms);
            pictograms = xmlParser.parse(xpp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(Pictogram pic : pictograms){
            createPictogram(pic, db);
        }
    }

    public long createPictogram(Pictogram pictogram, SQLiteDatabase db) {

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, pictogram.getCategory().toString());
        values.put(KEY_URL, pictogram.getUrl());

        // insert row
        long pictogram_id = db.insert(TABLE_PICTOGRAMS, null, values);

        for (String tag : pictogram.getTags()) {
            String[] parts = tag.split(Pictogram.LANGUAGE_SEPARATOR);
            addPictogramTag(pictogram_id, Constants.Language.valueOf(parts[0]), parts[1], db);
        }

        HashMap<Constants.Language, String> descriptions = pictogram.getDescriptions();
        for(Constants.Language language: Constants.Language.values()){
            String description = "";
            if(descriptions.containsKey(language))
                description = descriptions.get(language);
            setPictogramDescription(pictogram_id, language, description, db);
        }

        return pictogram_id;
    }

    public Pictogram getPictogramById(long pictogram_id, SQLiteDatabase db) {

        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAMS + " WHERE "
                + KEY_ID + " = " + pictogram_id + ";";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()){
            Pictogram.Category category = Pictogram.Category.valueOf(c.getString(c.getColumnIndex(KEY_CATEGORY)));
            String url = c.getString(c.getColumnIndex(KEY_URL));
            ArrayList<String> tags = getPictogramTags(pictogram_id, db);
            HashMap<Constants.Language, String> descriptions = getPictogramDescriptions(pictogram_id, db);
            Pictogram pictogram = new Pictogram(category, url, tags, descriptions);
            pictogram.setId(c.getInt(c.getColumnIndex(KEY_ID)));

            return pictogram;
        } else
            return null;

    }

    public ArrayList<Pictogram> getAllPictograms(SQLiteDatabase db) {
        ArrayList<Pictogram> pictograms = new ArrayList<Pictogram>();
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAMS + ";";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Pictogram.Category category = Pictogram.Category.valueOf(c.getString(c.getColumnIndex(KEY_CATEGORY)));
                String url = c.getString(c.getColumnIndex(KEY_URL));
                long pictogram_id = c.getInt(c.getColumnIndex(KEY_ID));
                ArrayList<String> tags = getPictogramTags(pictogram_id, db);
                HashMap<Constants.Language, String> descriptions = getPictogramDescriptions(pictogram_id, db);
                Pictogram pictogram = new Pictogram(category, url, tags, descriptions);
                pictogram.setId(pictogram_id);

                // adding to pictogram list
                pictograms.add(pictogram);
            } while (c.moveToNext());
        }

        return pictograms;
    }

    public ArrayList<Pictogram> getPictogramsByCategory(Pictogram.Category category, SQLiteDatabase db) {
        ArrayList<Pictogram> pictograms = new ArrayList<Pictogram>();
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAMS + " WHERE "
                + KEY_CATEGORY + " = '" + category.toString() + "';";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                String url = c.getString(c.getColumnIndex(KEY_URL));
                long pictogram_id = c.getInt(c.getColumnIndex(KEY_ID));
                ArrayList<String> tags = getPictogramTags(pictogram_id, db);
                HashMap<Constants.Language, String> descriptions = getPictogramDescriptions(pictogram_id, db);
                Pictogram pictogram = new Pictogram(category, url, tags, descriptions);
                pictogram.setId(pictogram_id);

                // adding to pictogram list
                pictograms.add(pictogram);
            } while (c.moveToNext());
        }

        return pictograms;
    }

    private HashMap<Constants.Language, String> getPictogramDescriptions(long pictogram_id, SQLiteDatabase db) {
        HashMap<Constants.Language, String> descriptions = new HashMap<Constants.Language, String>();
        for(Constants.Language language: Constants.Language.values()){
            descriptions.put(language, getPictogramDescription(pictogram_id, language, db));
        }
        return descriptions;
    }

    // ------------------------ "pictogram_tags" table methods ----------------//

    public long addPictogramTag(long pictogram_id, Constants.Language language, String tag, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(KEY_PICTOGRAM_ID, pictogram_id);
        values.put(KEY_LANGUAGE, language.toString());
        values.put(KEY_TAG, tag);

        // insert row
        long tag_id = db.insert(TABLE_PICTOGRAM_TAGS, null, values);

        return tag_id;
    }

    public ArrayList<String> getPictogramTags(long pictogram_id, SQLiteDatabase db){
        ArrayList<String> tags = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAM_TAGS + " WHERE "
                + KEY_PICTOGRAM_ID + " = " + pictogram_id + ";";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                String tag = c.getString(c.getColumnIndex(KEY_TAG));
                String language = c.getString(c.getColumnIndex(KEY_LANGUAGE));
                tags.add(language + Pictogram.LANGUAGE_SEPARATOR + tag);
            } while (c.moveToNext());
        }

        return tags;
    }

    public ArrayList<Pictogram> getPictogramsByTag(String tag, Constants.Language language, SQLiteDatabase db){
        ArrayList<Pictogram> pictograms = new ArrayList<Pictogram>();
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAM_TAGS + " WHERE "
                + KEY_LANGUAGE + " = '" + language.toString() + "' AND "
                + KEY_TAG + " = '" + tag + "';";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Pictogram pictogram = getPictogramById(c.getInt(c.getColumnIndex(KEY_PICTOGRAM_ID)), db);
                pictograms.add(pictogram);
            } while (c.moveToNext());
        }

        return pictograms;
    }

    // ------------------------ "pictogram_descriptions" table methods ----------------//

    public long setPictogramDescription(long pictogram_id, Constants.Language language, String description, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(KEY_PICTOGRAM_ID, pictogram_id);
        values.put(KEY_LANGUAGE, language.toString());
        values.put(KEY_DESCRIPTION, description);

        // update or insert row
        String[] args = new String[]{String.valueOf(pictogram_id), language.toString()};
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAM_DESCRIPTIONS + " WHERE "
                + KEY_LANGUAGE + " = '" + language.toString() + "' AND "
                + KEY_PICTOGRAM_ID + " = " + pictogram_id + ";";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        long description_id = -1;

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            description_id = db.update(TABLE_PICTOGRAM_DESCRIPTIONS, values, KEY_PICTOGRAM_ID + "=? AND " + KEY_LANGUAGE + "=?", args);
        } else {
            description_id = db.insert(TABLE_PICTOGRAM_DESCRIPTIONS, null, values);
        }

        return description_id;
    }

    public String getPictogramDescription(long pictogram_id, Constants.Language language, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + TABLE_PICTOGRAM_DESCRIPTIONS + " WHERE "
                + KEY_PICTOGRAM_ID + " = " + pictogram_id + " AND "
                + KEY_LANGUAGE + " = '" + language.toString() + "';";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()){
            String description = (c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
            return description;
        } else
            return null;
    }

    // ------------------------ "messages" table methods ----------------//

    public long createMessage(Message message, SQLiteDatabase db) {
        if(!conversationExists(message.getConversationId(), db)){
            Conversation conversation = new Conversation(message.getConversationUsername(), message.getConversationId(), message.getDate());
            createConversation(conversation, db);
        }

        ContentValues values = new ContentValues();
        values.put(KEY_PICTOGRAMS, message.serializePictograms());
        values.put(KEY_SENDER, message.getUserIsSender() ? TRUE : FALSE);
        values.put(KEY_CONVERSATION_ID, message.getConversationId());
        values.put(KEY_DATE, getDateAsString(message.getDate()));

        // insert row
        long message_id = db.insert(TABLE_MESSAGES, null, values);

        updateConversation(message, db);

        return message_id;
    }

    public ArrayList<Message> getMessagesOfConversation(String conversationId, String conversationUsername, SQLiteDatabase db) {
        ArrayList<Message> messages = new ArrayList<Message>();

        //get messages from most recent to oldest
        String selectQuery = "SELECT  * FROM " + TABLE_MESSAGES + " WHERE "
                + KEY_CONVERSATION_ID + " = '" + conversationId
                + "' ORDER BY " + KEY_DATE + " ASC;";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                ArrayList<String> pictograms = Message.deserializePictograms(c.getString(c.getColumnIndex(KEY_PICTOGRAMS)));
                boolean userIsSender = (c.getInt(c.getColumnIndex(KEY_SENDER)) == TRUE) ? true : false;
                Date date = getDateFromString(c.getString(c.getColumnIndex(KEY_DATE)));
                Message message = new Message(pictograms, userIsSender, conversationId, conversationUsername, date);
                message.setId(c.getInt((c.getColumnIndex(KEY_ID))));

                // adding to tags list
                messages.add(message);
            } while (c.moveToNext());
        }
        return messages;
    }

    public Message getLastMessageOfConversation(String conversationId, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS + " WHERE "
                + KEY_CONVERSATION_ID + " = '" + conversationId
                + "' ORDER BY " + KEY_LAST_MESSAGE + " DESC LIMIT 1;";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()){
            Log.e(LOG, "count c: " + c.getCount());

            Date date = getDateFromString(c.getString(c.getColumnIndex(KEY_LAST_MESSAGE)));
            String conversationUsername = (c.getString(c.getColumnIndex(KEY_USERNAME)));

            selectQuery = "SELECT * FROM " + TABLE_MESSAGES + " WHERE "
                    + KEY_CONVERSATION_ID + " = '" + conversationId + "' AND "
                    + KEY_DATE + " = '" + getDateAsString(date) + "';";

            c = db.rawQuery(selectQuery, null);

            if (c.moveToFirst()){
                Log.e(LOG, "count c: " + c.getCount());

                // If there are no messages, return null and treat this case in the controller
                if (c.getCount()==0)
                    return null;

                ArrayList<String> pictograms = Message.deserializePictograms(c.getString(c.getColumnIndex(KEY_PICTOGRAMS)));
                boolean userIsSender = (c.getInt(c.getColumnIndex(KEY_SENDER)) == TRUE) ? true : false;
                Message message = new Message(pictograms, userIsSender, conversationId, conversationUsername, date);
                message.setId(c.getInt((c.getColumnIndex(KEY_ID))));

                return message;

            } else
                return null;

        } else
            return null;
    }

    // ------------------------ "conversations" table methods ----------------//

    public void createConversation(Conversation conversation, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, conversation.getUsername());
        values.put(KEY_CONVERSATION_ID, conversation.getId());
        values.put(KEY_LAST_MESSAGE, getDateAsString(conversation.getLastMessage()));

        // insert row
        db.insert(TABLE_CONVERSATIONS, null, values);

        Log.e(LOG, "creating conversation");
    }

    public boolean conversationExists(String conversationId, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS + " WHERE "
                + KEY_CONVERSATION_ID + " = '" + conversationId + "';";

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst())
            return c.getCount()>0;
        else
            return false;
    }

    public ArrayList<Conversation> getAllConversations(SQLiteDatabase db){
        ArrayList<Conversation> conversations = new ArrayList<Conversation>();
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS
                + " ORDER BY " + KEY_LAST_MESSAGE + " DESC;";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                //conversation id, username, date
                String id = c.getString(c.getColumnIndex(KEY_CONVERSATION_ID));
                String user = c.getString(c.getColumnIndex(KEY_USERNAME));
                Date lastMessage = getDateFromString(c.getString(c.getColumnIndex(KEY_LAST_MESSAGE)));
                Conversation conversation = new Conversation(user, id, lastMessage);

                // adding to pictogram list
                conversations.add(conversation);
            } while (c.moveToNext());
        }

        return conversations;
    }

    //update last message date
    public void updateConversation(Message newMessage, SQLiteDatabase db){
        String conversation_id = newMessage.getConversationId();
        Date newDate = newMessage.getDate();
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS + " WHERE "
                + KEY_CONVERSATION_ID + " = '" + conversation_id + "';";

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()){
            Date oldDate = getDateFromString(c.getString(c.getColumnIndex(KEY_LAST_MESSAGE)));
            if(oldDate.before(newDate)){
                ContentValues newValues = new ContentValues();
                newValues.put(KEY_LAST_MESSAGE, getDateAsString(newDate));

                String[] args = new String[]{conversation_id};
                db.update(TABLE_CONVERSATIONS, newValues, KEY_CONVERSATION_ID + "=?", args);
            }
        }

    }

    public void deleteConversation(String conversation_id, SQLiteDatabase db) {
        db.delete(TABLE_MESSAGES, KEY_CONVERSATION_ID + " = ?",
                new String[] { conversation_id });
        db.delete(TABLE_CONVERSATIONS, KEY_CONVERSATION_ID + " = ?",
                new String[] { conversation_id });
    }

    // closing database
    public void closeDB(SQLiteDatabase db) {
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateAsString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }

    private Date getDateFromString(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
