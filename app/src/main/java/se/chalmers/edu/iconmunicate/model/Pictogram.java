package se.chalmers.edu.iconmunicate.model;

import java.util.ArrayList;
import java.util.HashMap;

import se.chalmers.edu.iconmunicate.util.Constants;

/**
 * Created by Markos Horro on 03/12/2015.
 */

public class Pictogram {

    public enum Category {
        COMM,
        EATABLE,
        LOCATION,
        MISC,
        TRANS
    }

    public static final String LANGUAGE_SEPARATOR = " ";

    private Category category;
    private String url;
    private HashMap<Constants.Language, String> descriptions;
    private ArrayList<String> tags;
    private long id;

    public Pictogram(Category category, String url, ArrayList<String> tags, HashMap<Constants.Language, String> descriptions) {
        this.category = category;
        this.url = url;
        this.tags = tags;
        this.descriptions = descriptions;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<Constants.Language, String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(HashMap<Constants.Language, String> descriptions) {
        this.descriptions = descriptions;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
