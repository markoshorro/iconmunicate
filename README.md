# README #

Brief description of the project

### iConmunication ###
This application would be used as a mean to communicate, when
people cannot understand each other in “standard ways”. A typical
use could be when the users do not speak any common language. The
idea would be to use pictograms instead of words. Each user has a list
of different icons, referring to different situations. These situations
can cover various field:

* Facilities: restaurant, tram station, toilets, etc.
* Basic communication vocabulary: eating, sleeping, etc.
* Typical questions: “where can I find. . . ?”, “do you have. . . ?”,
etc.

### Intended user ###

The intended user may vary widely, however it is obvious that the
application is meant for the people who is not able to communicate
each other. Some examples are described below.


### Functionality ###

Each user needs to have the application on his device. When they
meet each other, instead of trying to understand the signs of the other
person, the start the application. They can type a message, choosing a
succession of pictograms that would express their sentence as closely
as possible. The icons will be sorted in a clear list, making it very
user-friendly. We could imagine a brief description available on click
for every item, in case just an image is not enough. This description
would have to be available in several languages for obvious reasons.
When the message is complete, the user sends it to the interlocutor.
The latter can then see the pictograms, as long as their descriptions
in his own language.
To facilitate the communication, we will need to build a database
of pictograms representing a lot of situations. We will also need a
description for each of them in several languages, even if most of the
time icons would suffice.

###  Answering the five Ws? ###

* What: Being able to communicate with anyone, even though you may
not speak a common language. Using pictograms enables you to
understand without having to speak

* Why: Because there are a lot of situations where communication is
not easy, due to the difference of culture and language

* When: Any time you are meeting with someone you are not able to
understand

* Where: Anywhere you want to be able to communicate

* Who:  People who do not speak any common language: it can be two
people from different parts of the world (students, refugees, or
just random people), or maybe deaf/mute people